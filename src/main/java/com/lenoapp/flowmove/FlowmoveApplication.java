package com.lenoapp.flowmove;

import com.lenoapp.flowmove.webBrowserController.components.threadWatcher.FlowMoveThreadCaller;
import com.lenoapp.flowmove.webBrowserController.components.threadWatcher.threadClasses.seleniumThreads.SeleniumThreadClass;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;


@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
public class FlowmoveApplication {

    //id('items')/ytd-grid-video-renderer[2]/div[1]/ytd-thumbnail[1]/a[1]/div[2]/ytd-moving-thumbnail-renderer[1]/img[1]
    public static void main(String[] args) throws InterruptedException {
        SpringApplication.run(FlowmoveApplication.class, args);
    }

    public static void startGymMethod() {

    }



    private static void startTestWebPage(String url,String browser){
        FlowMoveThreadCaller.callThreadAndReturnId(SeleniumThreadClass.class, "startSeleniumWebDriver", false, webBrowserController, url, driver);
    }
}

