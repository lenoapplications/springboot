package com.lenoapp.flowmove.records.recordsExplorer.recordInfo;

public abstract class RecordInfoHolder {
    private String type;


    protected RecordInfoHolder(String type) {
        this.type = type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

}
