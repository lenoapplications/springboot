package com.lenoapp.flowmove.records.recordsExplorer.recordInfo;

public class RecordInfo extends RecordInfoHolder{
    private String dateCreated;

    public RecordInfo(String type) {
        super(type);
    }

    public String getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(String dateCreated) {
        this.dateCreated = dateCreated;
    }
}
