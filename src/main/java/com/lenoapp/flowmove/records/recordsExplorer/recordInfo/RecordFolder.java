package com.lenoapp.flowmove.records.recordsExplorer.recordInfo;

import java.util.HashMap;

public class RecordFolder extends RecordInfoHolder {
    private HashMap<String,RecordInfoHolder> recordInfoHolderHashMap;

    public RecordFolder(String type) {
        super(type);
    }

    public void setRecordInfoHolderHashMap(HashMap<String, RecordInfoHolder> recordInfoHolderHashMap) {
        this.recordInfoHolderHashMap = recordInfoHolderHashMap;
    }

    public HashMap<String, RecordInfoHolder> getRecordInfoHolderHashMap() {
        return recordInfoHolderHashMap;
    }

    public void addNewRecord(String key,RecordInfoHolder recordInfoHolder){
        recordInfoHolderHashMap.put(key,recordInfoHolder);
    }
}
