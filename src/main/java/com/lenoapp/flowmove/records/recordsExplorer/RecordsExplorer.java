package com.lenoapp.flowmove.records.recordsExplorer;


import com.lenoapp.flowmove.records.recordsExplorer.recordInfo.RecordFolder;
import com.lenoapp.flowmove.records.recordsExplorer.recordInfo.RecordInfo;
import com.lenoapp.flowmove.records.recordsExplorer.recordInfo.RecordInfoHolder;
import org.springframework.stereotype.Component;
import recorder.components.configuration.properties.ConfigurationProperties;
import recorder.configManager.ConfigManager;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.HashMap;

@Component
public class RecordsExplorer {
    private final File rootFoolder;

    public RecordsExplorer() {
        ConfigManager.initConfig();
        rootFoolder = new File(ConfigManager.getConfigurationParameter(ConfigurationProperties.RECORD_STORAGE_PATH));
    }

    public HashMap<String, RecordInfoHolder> getAllTestRecords(){
        HashMap<String,RecordInfoHolder> recordInfoHolderHashMap = new HashMap<>();
        RecordFolder recordFolder = searchForTestRecordsInFolder(rootFoolder);
        recordInfoHolderHashMap.put(rootFoolder.getName(),recordFolder);
        return recordInfoHolderHashMap;
    }


    private RecordFolder searchForTestRecordsInFolder(File folder){
        RecordFolder recordFolder = new RecordFolder("folder");
        recordFolder.setRecordInfoHolderHashMap(new HashMap<>());

        for (File file : folder.listFiles()){
            if (file.isDirectory()){
                String folderName = file.getName();
                RecordFolder recordSubFolder = searchForTestRecordsInFolder(file);
                recordFolder.addNewRecord(folderName,recordSubFolder);
            }else{
                RecordInfo recordInfo = createRecordInfo(file);
                recordFolder.addNewRecord(file.getName(),recordInfo);
            }
        }
        return recordFolder;

    }

    private RecordInfo createRecordInfo(File file){
        RecordInfo recordInfo = new RecordInfo("file");
        try {

            BasicFileAttributes attributes = Files.readAttributes(file.toPath(),BasicFileAttributes.class);
            recordInfo.setDateCreated(attributes.creationTime().toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return recordInfo;
    }
}
