package com.lenoapp.flowmove.webBrowserController.webElement;

import org.openqa.selenium.WebElement;
import recorder.SeleniumMaster;

public class BrowserAssistent {
    private final SeleniumMaster seleniumMaster;

    public BrowserAssistent(SeleniumMaster seleniumMaster) {
        this.seleniumMaster = seleniumMaster;
    }

    public WebElement getWebElementFromXpath(String xpath){
        return seleniumMaster.getWebDocumentController().getWebElements().getWebElement(xpath);
    }

    public void waitForPageToBeReady(){
        boolean ready = false;
        while(!ready){
            try {
                if (seleniumMaster.getWebDocumentController().getDocument().checkIfDocumentIsReady()){
                    ready = true;
                }
            }catch (Exception e){
            }
        }
    }
    public void changeFlagInJsFlagObject(String flag,Boolean value){
        seleniumMaster.getJsExecutorController().changeFlagInCode(flag,value);
    }
}
