package com.lenoapp.flowmove.webBrowserController.components.threadWatcher.threadClasses.seleniumThreads;


import com.lenoapp.flowmove.webBrowserController.WebBrowserController;
import thread_watcher.models.abstractions.user_method.UserMethod;
import thread_watcher.models.annotations.ThreadMethod;
import thread_watcher.user_parts.thread_bundle.Bundle;

public class SeleniumThreadClass extends UserMethod {
    private final String recorderString = "recorder";
    private final String urlTestSite = "url";
    private final String choosedDriver = "driver";

    @ThreadMethod(paramNames = {recorderString, urlTestSite,choosedDriver})
    public void startSeleniumWebDriver(Bundle bundle){
        WebBrowserController webBrowserController = (WebBrowserController) bundle.getArguments(recorderString);
        String url = (String) bundle.getArguments(urlTestSite);
        String driver = (String) bundle.getArguments(choosedDriver);
        webBrowserController.startNewTestRecord(url,driver);
    }
}
