package com.lenoapp.flowmove.webBrowserController.components.threadWatcher;

import async_communicator.thread_id_holder.ThreadIdHolder;
import thread_watcher.models.abstractions.user_method.UserMethod;
import thread_watcher.thread.caller.ThreadCaller;

public class FlowMoveThreadCaller {
    private final static FlowMoveThreadCaller flowMoveThreadCaller = new FlowMoveThreadCaller();
    private final ThreadCaller threadCaller = ThreadCaller.getThreadCaller();

    private FlowMoveThreadCaller(){}


    public static <A extends UserMethod>ThreadIdHolder callThreadAndReturnId(Class<A> userMethodClass,String methodName,boolean waitForFinish,Object...args) throws IllegalAccessException, InstantiationException {
        return new ThreadIdHolder(flowMoveThreadCaller.threadCaller.callThread(userMethodClass,methodName,args));
    }
}
