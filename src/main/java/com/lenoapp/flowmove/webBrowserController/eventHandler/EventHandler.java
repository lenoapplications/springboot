package com.lenoapp.flowmove.webBrowserController.eventHandler;

import recorder.components.events.eventHolder.eventsImpl.dao.DocumentEventListenerDao;
import recorder.components.records.recordHolder.RecordHolder;

public class EventHandler {
    private RecordHolder recordHolder;


    public void turnOnEventHandler(RecordHolder recordHolder){
        this.recordHolder = recordHolder;
    }

    public void addNewEvent(DocumentEventListenerDao documentEventListenerDao){
        if (recordHolder != null){
            recordHolder.addNewEvent(documentEventListenerDao);
        }
    }

    public void reset(){
        recordHolder = null;
    }

}

