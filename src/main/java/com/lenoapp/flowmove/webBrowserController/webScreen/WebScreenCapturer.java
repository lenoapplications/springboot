package com.lenoapp.flowmove.webBrowserController.webScreen;

import org.openqa.selenium.WebElement;
import recorder.SeleniumMaster;

import java.awt.*;
import java.awt.image.BufferedImage;

public class WebScreenCapturer {
    private final WebScreenImageHolder webScreenImageHolder;
    private final SeleniumMaster seleniumMaster;

    public WebScreenCapturer(SeleniumMaster seleniumMaster) {
        this.seleniumMaster = seleniumMaster;
        this.webScreenImageHolder = new WebScreenImageHolder();

    }
    public void takeNewWebScreen(){
        webScreenImageHolder.setCurrentWebScreen(seleniumMaster.getDriverScreenShotTaker().getScreenShotBufferedImage());
    }
    public byte[] getMarkedElementOnCurrentWebScreen(WebElement webElement){
        return seleniumMaster.getDriverScreenShotTaker().markWebElementOnImageAndReturnByteArray(copyInstanceOfBufferedImage(),webElement);
    }

    public byte[] getCurrentWebScreen(){
        return seleniumMaster.getDriverScreenShotTaker().getBufferedImageToByte(webScreenImageHolder.getCurrentWebScreen());
    }

    public byte[] getMarkedElementOnNewWebScreen(WebElement webElement){
        seleniumMaster.getWebDocumentController().getWebElements().scrollIntoView(webElement);
        return seleniumMaster.getDriverScreenShotTaker().getScreenShotWithMarkedOfWebElement(webElement);
    }


    private BufferedImage copyInstanceOfBufferedImage(){
        BufferedImage source = webScreenImageHolder.getCurrentWebScreen();
        BufferedImage copy = new BufferedImage(source.getWidth(), source.getHeight(), source.getType());
        Graphics g = copy.getGraphics();
        g.drawImage(source, 0, 0, null);
        g.dispose();
        return copy;
    }

}
