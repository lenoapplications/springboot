package com.lenoapp.flowmove.webBrowserController.webScreen;

import java.awt.image.BufferedImage;

public class WebScreenImageHolder {
    private BufferedImage currentWebScreen;

    public WebScreenImageHolder(){

    }
    public void setCurrentWebScreen(BufferedImage currentWebScreen) {
        this.currentWebScreen = currentWebScreen;
    }

    public BufferedImage getCurrentWebScreen() {
        return currentWebScreen;
    }
}
