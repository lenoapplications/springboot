package com.lenoapp.flowmove.webBrowserController;

import com.lenoapp.flowmove.webBrowserController.eventHandler.EventHandler;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.JsUserApi;
import com.lenoapp.flowmove.webBrowserController.webElement.BrowserAssistent;
import com.lenoapp.flowmove.webBrowserController.webScreen.WebScreenCapturer;
import org.springframework.stereotype.Component;
import recorder.SeleniumMaster;
import recorder.components.records.recordHolder.RecordHolder;


@Component
public class WebBrowserController {
    private final EventHandler eventHandler;

    private final SeleniumMaster seleniumMaster;
    private final BrowserAssistent browserAssistent;
    private final WebScreenCapturer webScreenCapturer;

    public WebBrowserController() {
        this.seleniumMaster = new SeleniumMaster();
        this.eventHandler = new EventHandler();
        this.browserAssistent = new BrowserAssistent(seleniumMaster);
        this.webScreenCapturer = new WebScreenCapturer(seleniumMaster);

    }

    public void startNewTestRecord(String url,String choosedDriver){
        RecordHolder recordHolder = seleniumMaster.startNewTestRecord(url,choosedDriver);
        eventHandler.turnOnEventHandler(recordHolder);
    }
    public void saveTestRecord(String filename){
        eventHandler.reset();
        seleniumMaster.saveTestRecord(filename);
    }


    public boolean isRecording(){
        return seleniumMaster.isRecording();
    }


    public void setupJsUserApiCall(JsUserApi jsUserApi){
        jsUserApi.setSeleniumMaster(seleniumMaster);
    }

    public WebScreenCapturer getWebScreenCapturer() {
        return webScreenCapturer;
    }

    public BrowserAssistent getBrowserAssistent() {
        return browserAssistent;
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }
}
