package com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassCallers;

import com.lenoapp.flowmove.spring.model.domain.dto.jsUserResponse.JsUserApiResponseDto;
import recorder.SeleniumMaster;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public abstract class JsUserApiClassCallerModel<A> {
    protected final SeleniumMaster seleniumMaster;
    private A classObject;

    protected JsUserApiClassCallerModel(SeleniumMaster seleniumMaster) {
        this.seleniumMaster = seleniumMaster;
    }
    protected void setClassObject(A classObject) {
        this.classObject = classObject;
    }

    private Class<Object>[] getClassArgs(int size,Object...args){
        Class[] objectArgs = new Class[size];
        int index = 0;
        for (Object object : args){
            objectArgs[index] = object.getClass();
        }
        return objectArgs;
    }


    public JsUserApiResponseDto callFunction(String functionCaller,Object...args) {
        try {
            Method methodToCall = this.getClass().getMethod(functionCaller,getClassArgs(args.length,args));
            return (JsUserApiResponseDto) methodToCall.invoke(this.classObject,args);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }
}
