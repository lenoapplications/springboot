package com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassCallers.jsClassCallersImpl;

import com.lenoapp.flowmove.spring.model.domain.dto.jsUserResponse.JsUserApiResponseDto;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassCallers.JsUserApiClassCallerModel;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassIdent.JsDocumentApiIdent;
import recorder.SeleniumMaster;

public class JsDocumentApiCaller extends JsUserApiClassCallerModel {

    public JsDocumentApiCaller(SeleniumMaster seleniumMaster) {
        super(seleniumMaster);
        setClassObject(this);
    }

    public JsUserApiResponseDto<String> getDocumentBodyToString(){
        String tagFunction =  JsDocumentApiIdent.GET_DOCUMENT_BODY_FUNC_TAG.getTag();
        String javascriptCaller = JsDocumentApiIdent.GET_DOCUMENT_BODY_FUNC_TAG.getJavascriptCaller();
        String documentBody = seleniumMaster.getJsExecutorController().uploadUserApiCode(JsDocumentApiIdent.SRC_FILE.getTag(),tagFunction,javascriptCaller);
        JsUserApiResponseDto<String> jsUserApiResponseDto = new JsUserApiResponseDto<String>(documentBody);
        return jsUserApiResponseDto;
    }



}
