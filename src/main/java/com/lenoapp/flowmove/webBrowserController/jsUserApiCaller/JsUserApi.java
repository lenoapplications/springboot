package com.lenoapp.flowmove.webBrowserController.jsUserApiCaller;

import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassCallers.JsUserApiClassCallerModel;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassCallers.jsClassCallersImpl.JsDocumentApiCaller;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassCallers.jsClassCallersImpl.JsWebElementApiCaller;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassIdent.JsDocumentApiIdent;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassIdent.JsWebElementApiIdent;
import org.springframework.stereotype.Component;
import recorder.SeleniumMaster;

import java.util.HashMap;


@Component
public class JsUserApi {
    private final  HashMap<String, JsUserApiClassCallerModel> jsUserApiClassCallerModelHashMap;

    public JsUserApi() {
        jsUserApiClassCallerModelHashMap = new HashMap<>();
    }

    public void setSeleniumMaster(SeleniumMaster seleniumMaster) {
        setJsUserApiClassCallerModelHashMap(seleniumMaster);
    }

    public <A extends JsUserApiClassCallerModel> A getJsUserApiClassCaller(String srcFile){
        if (jsUserApiClassCallerModelHashMap.containsKey(srcFile)){
            return (A) jsUserApiClassCallerModelHashMap.get(srcFile);
        }else{
            throw new NullPointerException("Js user class doesn't exist");
        }
    }


    private void setJsUserApiClassCallerModelHashMap(SeleniumMaster seleniumMaster){
        jsUserApiClassCallerModelHashMap.put(JsDocumentApiIdent.SRC_FILE.getTag(),new JsDocumentApiCaller(seleniumMaster));
        jsUserApiClassCallerModelHashMap.put(JsWebElementApiIdent.SRC_FILE.getTag(),new JsWebElementApiCaller(seleniumMaster));
    }


}
