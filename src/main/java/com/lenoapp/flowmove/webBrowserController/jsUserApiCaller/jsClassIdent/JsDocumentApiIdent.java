package com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassIdent;

public enum JsDocumentApiIdent {
    SRC_FILE("DocumentApi.js","",""),

    GET_DOCUMENT_BODY_FUNC_TAG("GET_DOCUMENT__BODY_STRING","getDocumentBodyToString","return getDocumentBodyToString()")
    ;

    private String tag;
    private String javaCaller;
    private String javascriptCaller;

    JsDocumentApiIdent(String tag, String javaCaller, String javascriptCaller){
        this.tag = tag;
        this.javaCaller = javaCaller;
        this.javascriptCaller = javascriptCaller;
    }

    public String getTag() {
        return tag;
    }

    public String getJavaCaller() {
        return getJavaCaller();
    }

    public String getJavascriptCaller() {
        return javascriptCaller;
    }
}
