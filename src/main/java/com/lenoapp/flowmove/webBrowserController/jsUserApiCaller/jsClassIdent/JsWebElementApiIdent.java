package com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassIdent;

public enum JsWebElementApiIdent {
    SRC_FILE("WebElementApi.js","",""),

    GET_WEB_ELEMENT_STYLES("GET_WEB_ELEMENT_STYLES","getWebElementStyles","return getWebElementStyles(arguments[0])"),
    SCROLL_INTO_VIEW_WEB_ELEMENT("SCROLL_INTO_VIEW_WEB_ELEMENT","scrollIntoViewWebElement","scrollIntoViewWebElement( arguments[0] )")
    ;

    private String tag;
    private String javaCaller;
    private String javascriptCaller;

    JsWebElementApiIdent(String tag, String javaCaller, String javascriptCaller){
        this.tag = tag;
        this.javaCaller = javaCaller;
        this.javascriptCaller = javascriptCaller;
    }

    public String getTag() {
        return tag;
    }

    public String getJavaCaller() {
        return getJavaCaller();
    }

    public String getJavascriptCaller() {
        return javascriptCaller;
    }
}
