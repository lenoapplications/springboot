package com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassCallers.jsClassCallersImpl;

import com.lenoapp.flowmove.spring.model.domain.dto.jsUserResponse.JsUserApiResponseDto;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassCallers.JsUserApiClassCallerModel;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.jsClassIdent.JsWebElementApiIdent;
import org.openqa.selenium.WebElement;
import recorder.SeleniumMaster;

import java.util.Map;

public class JsWebElementApiCaller extends JsUserApiClassCallerModel<JsWebElementApiCaller> {

    public JsWebElementApiCaller(SeleniumMaster seleniumMaster) {
        super(seleniumMaster);
        setClassObject(this);
    }

    public JsUserApiResponseDto getWebElementStyles(String xpath){
        String srcFile = JsWebElementApiIdent.SRC_FILE.getTag();
        String tagFunction = JsWebElementApiIdent.GET_WEB_ELEMENT_STYLES.getTag();
        String javascriptCaller = JsWebElementApiIdent.GET_WEB_ELEMENT_STYLES.getJavascriptCaller();
        Map<String,Object> result = (Map<String,Object>)seleniumMaster.getJsExecutorController().uploadUserApiCode(srcFile,tagFunction,javascriptCaller,xpath);
        return new JsUserApiResponseDto(result);
    }
    public JsUserApiResponseDto scrollIntoViewWebElement(WebElement webElement){
        String srcFile = JsWebElementApiIdent.SRC_FILE.getTag();
        String tagFunction = JsWebElementApiIdent.SCROLL_INTO_VIEW_WEB_ELEMENT.getTag();
        String javascriptCaller = JsWebElementApiIdent.SCROLL_INTO_VIEW_WEB_ELEMENT.getJavascriptCaller();
        seleniumMaster.getJsExecutorController().uploadUserApiCode(srcFile,tagFunction,javascriptCaller,webElement);
        return new JsUserApiResponseDto(true);
    }

}
