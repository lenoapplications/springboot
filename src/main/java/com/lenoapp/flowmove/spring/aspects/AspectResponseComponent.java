package com.lenoapp.flowmove.spring.aspects;


import com.lenoapp.flowmove.spring.model.annotations.GeneralResponse;
import com.lenoapp.flowmove.spring.model.annotations.JsUserApiResponse;
import com.lenoapp.flowmove.spring.model.domain.dto.general.GeneralResponseDto;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.JsUserApi;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

@Aspect
@Component
public class AspectResponseComponent {

    @Autowired
    private JsUserApi jsUserApi;


    @Around("@annotation(com.lenoapp.flowmove.spring.model.annotations.GeneralResponse) && execution(* *(..))")
    public GeneralResponseDto sendResponse(JoinPoint pjp) throws Throwable{
        ProceedingJoinPoint proceedingJoinPoint = (ProceedingJoinPoint) pjp;
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = methodSignature.getMethod();

        GeneralResponse generalResponseAnnotation = method.getAnnotation(com.lenoapp.flowmove.spring.model.annotations.GeneralResponse.class);
        GeneralResponseDto generalResponse = new GeneralResponseDto();
        generalResponse.setResponse( generalResponseAnnotation.response() );
        generalResponse.setDestination( generalResponseAnnotation.destination() );
        return generalResponse;

    }


    @Around("@annotation(com.lenoapp.flowmove.spring.model.annotations.JsUserApiResponse) && execution(* *(..))")
    public GeneralResponseDto sendJsResponse(JoinPoint point) throws Throwable{
        ProceedingJoinPoint proceedingJoinPoint = (ProceedingJoinPoint) point;
        MethodSignature methodSignature = (MethodSignature) proceedingJoinPoint.getSignature();
        Method method = methodSignature.getMethod();
        Object[] args = proceedingJoinPoint.getArgs();

        JsUserApiResponse jsUserApiResponse = method.getAnnotation(JsUserApiResponse.class);
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        String srcFile = jsUserApiResponse.srcFile().concat(".js");
        String javaCaller = jsUserApiResponse.javaCaller();

        generalResponseDto.setResponse(jsUserApi.getJsUserApiClassCaller(srcFile).callFunction(javaCaller,args));
        generalResponseDto.setDestination(javaCaller);
        return generalResponseDto;
    }
}
