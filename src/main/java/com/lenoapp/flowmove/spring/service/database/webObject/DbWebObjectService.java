package com.lenoapp.flowmove.spring.service.database.webObject;


import com.lenoapp.flowmove.spring.model.domain.database.collections.webObject.WebObject;
import com.lenoapp.flowmove.spring.model.domain.database.repositories.interfaces.webObject.WebObjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DbWebObjectService {

    @Autowired
    WebObjectRepository webObjectRepository;


    public WebObject getWebObjectWithName(String name){
        return webObjectRepository.findWebObjectByWebName(name);
    }
    public List<WebObject> getAllWebObjects(){
        return webObjectRepository.findAll();
    }

    public void saveWebObject(WebObject webObject){
        webObjectRepository.save(webObject);
    }
}
