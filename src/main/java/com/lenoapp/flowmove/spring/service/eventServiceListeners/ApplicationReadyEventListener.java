package com.lenoapp.flowmove.spring.service.eventServiceListeners;

import com.lenoapp.flowmove.webBrowserController.WebBrowserController;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.JsUserApi;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;


@Component
public class ApplicationReadyEventListener{

    @EventListener
    public void onApplicationEvent(ContextRefreshedEvent event) {
        WebBrowserController webBrowserController = (WebBrowserController) event.getApplicationContext().getAutowireCapableBeanFactory().resolveNamedBean(WebBrowserController.class).getBeanInstance();
        JsUserApi jsUserApi = (JsUserApi)event.getApplicationContext().getAutowireCapableBeanFactory().resolveNamedBean(JsUserApi.class).getBeanInstance();

        webBrowserController.setupJsUserApiCall(jsUserApi);
    }
}

