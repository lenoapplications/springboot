package com.lenoapp.flowmove.spring.model.domain.database.collections.webObject;

import com.lenoapp.flowmove.spring.model.domain.dto.database.webObject.WebObjectDto;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;

@Document(collection = "webObject")
public class WebObject {

    @Id
    public String id;

    public String webName;
    public String url;
    public WebObjectSelectors webObjectSelectors;

    public WebObject(){

    }

    public WebObject(String webName,WebObjectSelectors webObjectSelectors){
        this.webName = webName;
        this.webObjectSelectors = webObjectSelectors;
    }
    public WebObject(WebObjectDto webObjectDto){
        this.webName = webObjectDto.getWebName();
        this.url = webObjectDto.getUrl();
        this.webObjectSelectors = new WebObjectSelectors();
    }


}
