package com.lenoapp.flowmove.spring.model.domain.dto.qualityCenter;

import recorder.components.events.assertEventHolder.SaveAssertAttributeList;
import recorder.components.events.eventHolder.eventsImpl.dto.DocumentEventListenersDto;

public class StepDto {
    private String stepTitle;
    private DocumentEventListenersDto documentEventListenersDto;
    private SaveAssertAttributeList saveAssertAttributeList;


    public SaveAssertAttributeList getSaveAssertAttributeList() {
        return saveAssertAttributeList;
    }

    public void setSaveAssertAttributeList(SaveAssertAttributeList saveAssertAttributeList) {
        this.saveAssertAttributeList = saveAssertAttributeList;
    }

    public void setDocumentEventListenersDto(DocumentEventListenersDto documentEventListenersDto){
        this.documentEventListenersDto = documentEventListenersDto;
    }
    public DocumentEventListenersDto getDocumentEventListenersDto() {
        return documentEventListenersDto;
    }
    public String getStepTitle() {
        return stepTitle;
    }

    public void setStepTitle(String stepTitle) {
        this.stepTitle = stepTitle;
    }

}
