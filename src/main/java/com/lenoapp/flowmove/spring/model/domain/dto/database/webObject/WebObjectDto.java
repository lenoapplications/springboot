package com.lenoapp.flowmove.spring.model.domain.dto.database.webObject;

import java.util.List;

public class WebObjectDto {
    private String webName;
    private String url;
    private List<String> selectors;



    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWebName() {
        return webName;
    }

    public void setWebName(String webName) {
        this.webName = webName;
    }

    public List<String> getSelectors() {
        return selectors;
    }

    public void setSelectors(List<String> selectors) {
        this.selectors = selectors;
    }
}
