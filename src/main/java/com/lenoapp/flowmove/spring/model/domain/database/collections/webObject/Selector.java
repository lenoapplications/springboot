package com.lenoapp.flowmove.spring.model.domain.database.collections.webObject;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;


@Document(collection = "selector")
public class Selector {

    @Id
    public String id;

    public String name;
    public String xpath;

    public Selector(String name, String xpath){
        this.name = name;
        this.xpath = xpath;
    }
}
