package com.lenoapp.flowmove.spring.model.domain.dto.general;

public class GeneralResponseDto<A> {
    private String destination;
    private A response;

    public A getResponse() {
        return response;
    }

    public void setResponse(A response) {
        this.response = response;
    }
    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
