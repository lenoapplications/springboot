package com.lenoapp.flowmove.spring.model.domain.database.repositories.interfaces.webObject;

import com.lenoapp.flowmove.spring.model.domain.database.collections.webObject.WebObject;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WebObjectRepository extends MongoRepository<WebObject,String> {

    WebObject findWebObjectByWebName(String webName);

}
