package com.lenoapp.flowmove.spring.model.domain.dto.jsUserResponse;

public class JsUserApiResponseDto<A> {
    private A returnValue;
    private String responseStatus;

    public JsUserApiResponseDto(A returnValue){
        this.returnValue = returnValue;
    }

    public String getResponseStatus() {
        return responseStatus;
    }

    public void setResponseStatus(String responseStatus) {
        this.responseStatus = responseStatus;
    }


    public A getReturnValue() {
        return returnValue;
    }

    public void setReturnValue(A returnValue) {
        this.returnValue = returnValue;
    }
}
