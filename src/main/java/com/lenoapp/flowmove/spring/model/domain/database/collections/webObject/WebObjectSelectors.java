package com.lenoapp.flowmove.spring.model.domain.database.collections.webObject;

import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Document(collection = "webObjectSelectors")
public class WebObjectSelectors {

    @Id
    public String id;
    public List<String> selectors;


    public WebObjectSelectors(){
        this.selectors = new ArrayList<>();
    }

    public WebObjectSelectors(List<String> selectors){
        this.selectors = selectors;
    }
}
