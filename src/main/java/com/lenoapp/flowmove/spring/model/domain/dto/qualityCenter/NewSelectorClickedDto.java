package com.lenoapp.flowmove.spring.model.domain.dto.qualityCenter;

public class NewSelectorClickedDto {
    private String htmlDoc;
    private String xpath;


    public NewSelectorClickedDto(String xpath,String htmlDoc){
        this.htmlDoc = htmlDoc;
        this.xpath = xpath;
    }

    public String getHtmlDoc() {
        return htmlDoc;
    }

    public void setHtmlDoc(String htmlDoc) {
        this.htmlDoc = htmlDoc;
    }

    public String getXpath() {
        return xpath;
    }

    public void setXpath(String xpath) {
        this.xpath = xpath;
    }

}
