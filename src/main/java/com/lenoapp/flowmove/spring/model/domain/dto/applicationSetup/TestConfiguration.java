package com.lenoapp.flowmove.spring.model.domain.dto.applicationSetup;

public class TestConfiguration {
    private String browser;
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }


    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }
}
