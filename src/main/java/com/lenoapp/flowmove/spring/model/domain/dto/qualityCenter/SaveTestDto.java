package com.lenoapp.flowmove.spring.model.domain.dto.qualityCenter;

import java.util.List;

public class SaveTestDto {
    private List<StepDto> stepDtoList;

    public List<StepDto> getStepDtoList() {
        return stepDtoList;
    }

    public void setStepDtoList(List<StepDto> stepDtoList) {
        this.stepDtoList = stepDtoList;
    }
}
