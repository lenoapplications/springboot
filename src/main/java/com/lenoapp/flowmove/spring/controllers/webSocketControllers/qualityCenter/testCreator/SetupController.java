package com.lenoapp.flowmove.spring.controllers.webSocketControllers.qualityCenter.testCreator;


import com.lenoapp.flowmove.spring.model.domain.dto.general.GeneralResponseDto;
import com.lenoapp.flowmove.webBrowserController.WebBrowserController;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.JsUserApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class SetupController {

    @Autowired
    private WebBrowserController webBrowserController;
    @Autowired
    private JsUserApi jsUserApi;


    /***
     * INITIALIZATION
     * ***/
    @MessageMapping("/qualityCenter/testCreator/setupController/initialization")
    @SendTo("/flowMove//qualityCenter/testCreator/setupController/setupControllerResponse")
    public GeneralResponseDto initialization(){
        System.out.println("Initialization controller");
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        generalResponseDto.setDestination("initialization");

        webBrowserController.getBrowserAssistent().waitForPageToBeReady();
        webBrowserController.getWebScreenCapturer().takeNewWebScreen();

        generalResponseDto.setResponse("Document is ready");
        return generalResponseDto;
    }

}
