package com.lenoapp.flowmove.spring.controllers.webSocketControllers.applicationSetup;


import com.lenoapp.flowmove.spring.model.annotations.GeneralResponse;
import com.lenoapp.flowmove.spring.model.domain.dto.applicationSetup.TestConfiguration;
import com.lenoapp.flowmove.spring.model.domain.dto.general.GeneralResponseDto;
import com.lenoapp.flowmove.spring.service.database.webObject.DbWebObjectService;
import com.lenoapp.flowmove.webBrowserController.WebBrowserController;
import com.lenoapp.flowmove.webBrowserController.components.threadWatcher.FlowMoveThreadCaller;
import com.lenoapp.flowmove.webBrowserController.components.threadWatcher.threadClasses.seleniumThreads.SeleniumThreadClass;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class ApplicationSetupController {

    @Autowired
    private WebBrowserController webBrowserController;

    @Autowired
    private DbWebObjectService dbWebObjectService;


    @MessageMapping("/applicationSetup/applicationSetupController/startApplication")
    @SendTo("/flowMove/applicationSetup/applicationSetupController/applicationSetupControllerResponse")
    @GeneralResponse(response = "Connected",destination = "startApplication")
    public Object startApplication(){
        return null;
    }

    @MessageMapping("/applicationSetup/applicationSetupController/startWebDriver")
    @SendTo("/flowMove/applicationSetup/applicationSetupController/applicationSetupControllerResponse")
    public GeneralResponseDto startWebDriver(TestConfiguration testRecordConfiguration){
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        generalResponseDto.setDestination("startWebDriver");

        try {
            String url = testRecordConfiguration.getUrl();
            String driver = testRecordConfiguration.getBrowser();
            FlowMoveThreadCaller.callThreadAndReturnId(SeleniumThreadClass.class, "startSeleniumWebDriver", false, webBrowserController, url, driver);
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
            generalResponseDto.setResponse("Web driver run failed");
            return generalResponseDto;
        }
        System.out.println("Ovdje sam kod webStartDrivererere");
        generalResponseDto.setResponse("Web driver started");
        return generalResponseDto;
    }


}
