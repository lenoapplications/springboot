package com.lenoapp.flowmove.spring.controllers.webSocketControllers.qualityCenter.testCreator;


import com.lenoapp.flowmove.spring.model.domain.dto.general.GeneralResponseDto;
import com.lenoapp.flowmove.spring.model.domain.dto.jsUserResponse.JsUserApiResponseDto;
import com.lenoapp.flowmove.spring.model.domain.dto.qualityCenter.NewSelectorClickedDto;
import com.lenoapp.flowmove.webBrowserController.WebBrowserController;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.JsUserApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class SelectorController {

    @Autowired
    private WebBrowserController webBrowserController;
    @Autowired
    private JsUserApi jsUserApi;




    /***
     * SELECTORS CONTROL POINTS
     *
     * newSelectorClickedResponse
     */
    @MessageMapping("/qualityCenter/testCreator/selectorController/selectorClicked")
    @SendTo("/flowMove/qualityCenter/testCreator/selectorController/selectorControllerResponse")
    public GeneralResponseDto newSelectorClickedEvent(String xpath){
        System.out.println("selector clicked  "+xpath);
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        String srcFile = "DocumentApi.js";
        String javaCaller = "getDocumentBodyToString";
        JsUserApiResponseDto jsUserApiResponseDto = jsUserApi.getJsUserApiClassCaller(srcFile).callFunction(javaCaller);

        generalResponseDto.setResponse(new NewSelectorClickedDto(xpath,(String)jsUserApiResponseDto.getReturnValue()));
        generalResponseDto.setDestination("selectorClicked");
        return generalResponseDto;
    }

}
