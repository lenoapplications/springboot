package com.lenoapp.flowmove.spring.controllers.webSocketControllers.qualityCenter.testCreator;


import com.lenoapp.flowmove.spring.model.domain.dto.general.GeneralResponseDto;
import com.lenoapp.flowmove.webBrowserController.WebBrowserController;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.JsUserApi;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebScreenController {

    @Autowired
    private WebBrowserController webBrowserController;
    @Autowired
    private JsUserApi jsUserApi;

    /***
     *
     * WEB SCREEN CONTROL POINTS
     * ***/

    @MessageMapping("/qualityCenter/testCreator/webScreenController/getWebScreen")
    @SendTo("/flowMove/qualityCenter/testCreator/webScreenController/webScreenControllerResponse")
    public GeneralResponseDto getWebScreenShot(){
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        generalResponseDto.setResponse(webBrowserController.getWebScreenCapturer().getCurrentWebScreen());
        generalResponseDto.setDestination("getWebScreen");
        return generalResponseDto;
    }

    @MessageMapping("/qualityCenter/testCreator/webScreenController/getWebScreenWithMarkedWebElement")
    @SendTo("/flowMove/qualityCenter/testCreator/webScreenController/webScreenControllerResponse")
    public GeneralResponseDto getWebScreenShotWithMarkedWebElement(String xpath){
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        WebElement webElement = webBrowserController.getBrowserAssistent().getWebElementFromXpath(xpath);

        generalResponseDto.setResponse(webBrowserController.getWebScreenCapturer().getMarkedElementOnNewWebScreen(webElement));
        generalResponseDto.setDestination("getWebScreenWithMarkedWebElement");

        return generalResponseDto;
    }

}
