package com.lenoapp.flowmove.spring.controllers.webSocketControllers.qualityCenter.testCreator;


import com.lenoapp.flowmove.spring.model.annotations.JsUserApiResponse;
import com.lenoapp.flowmove.spring.model.domain.dto.general.GeneralResponseDto;
import com.lenoapp.flowmove.webBrowserController.WebBrowserController;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.JsUserApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import recorder.components.configuration.flags.JavascriptFlags;

@Controller
public class JsDocumentController {

    @Autowired
    private WebBrowserController webBrowserController;
    @Autowired
    private JsUserApi jsUserApi;




    /***
     * DOCUMENT CONTROL POINTS
     */
    @MessageMapping("/qualityCenter/testCreator/jsDocumentController/documentBody")
    @SendTo("/flowMove/qualityCenter/testCreator/jsDocumentController/jsDocumentControllerResponse")
    @JsUserApiResponse(srcFile = "DocumentApi", javaCaller = "getDocumentBodyToString")
    public Object jsUserApiFunctionGetDocumentBodyToString(){
        return null;
    }

    @MessageMapping("/qualityCenter/testCreator/jsDocumentController/activateSearchForSelector")
    @SendTo("/flowMove/qualityCenter/testCreator/jsDocumentController/jsDocumentControllerResponse")
    public GeneralResponseDto activateSearchForSelector(){
        webBrowserController.getBrowserAssistent().changeFlagInJsFlagObject(JavascriptFlags.SEARCH_FOR_SELECTOR.getProperty(),true);
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        generalResponseDto.setResponse("Activated");
        generalResponseDto.setDestination("activationSearchSelector");
        return generalResponseDto;
    }

    @MessageMapping("/qualityCenter/testCreator/jsDocumentController/deactivateSearchForSelector")
    @SendTo("/flowMove/qualityCenter/testCreator/jsDocumentController/jsDocumentControllerResponse")
    public GeneralResponseDto deactivateSearchForSelector(){
        webBrowserController.getBrowserAssistent().changeFlagInJsFlagObject(JavascriptFlags.SEARCH_FOR_SELECTOR.getProperty(),false);
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        generalResponseDto.setResponse("Deactivated");
        generalResponseDto.setDestination("activationSearchSelector");
        return generalResponseDto;
    }

}
