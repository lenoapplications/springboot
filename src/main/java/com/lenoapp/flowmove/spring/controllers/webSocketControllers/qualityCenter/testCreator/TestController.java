package com.lenoapp.flowmove.spring.controllers.webSocketControllers.qualityCenter.testCreator;


import com.lenoapp.flowmove.spring.model.domain.dto.general.GeneralResponseDto;
import com.lenoapp.flowmove.spring.model.domain.dto.qualityCenter.SaveTestDto;
import com.lenoapp.flowmove.spring.model.domain.dto.qualityCenter.StepDto;
import com.lenoapp.flowmove.webBrowserController.WebBrowserController;
import com.lenoapp.flowmove.webBrowserController.jsUserApiCaller.JsUserApi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import recorder.components.events.eventHolder.eventsImpl.dao.DocumentEventListenerDao;

@Controller
public class TestController {

    @Autowired
    private WebBrowserController webBrowserController;
    @Autowired
    private JsUserApi jsUserApi;





    /**
     *
     * RECORD CONTROL POINTS
     */
    @MessageMapping("/qualityCenter/testController/saveTest")
    @SendTo("/flowMove/qualityCenter/testController/saveTestResponse")
    public GeneralResponseDto saveTest(SaveTestDto steps){
        for(StepDto stepDto : steps.getStepDtoList()){
            System.out.println(stepDto.getStepTitle());
            DocumentEventListenerDao documentEventListenerDao = new DocumentEventListenerDao(stepDto.getDocumentEventListenersDto(),stepDto.getStepTitle());
            documentEventListenerDao.getAssertionSerializableEvents().setSaveAssertAttributeList(stepDto.getSaveAssertAttributeList());
            webBrowserController.getEventHandler().addNewEvent(documentEventListenerDao);
        }

        if (webBrowserController.isRecording()){
            webBrowserController.saveTestRecord("FILENAME");
        }

        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        generalResponseDto.setDestination("saveTest");
        generalResponseDto.setResponse("Test saved");
        System.out.println("record saved");
        return generalResponseDto;
    }




}
