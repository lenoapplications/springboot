package com.lenoapp.flowmove.spring.controllers.webSocketControllers.applicationSetup;


import com.lenoapp.flowmove.spring.model.domain.database.collections.webObject.WebObject;
import com.lenoapp.flowmove.spring.model.domain.dto.database.webObject.WebObjectDto;
import com.lenoapp.flowmove.spring.model.domain.dto.general.GeneralResponseDto;
import com.lenoapp.flowmove.spring.service.database.webObject.DbWebObjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class WebObjectCreatorController {

    @Autowired
    private DbWebObjectService dbWebObjectService;


    @MessageMapping("/applicationSetup/webObjectCreatorController/createWebObject")
    @SendTo("/flowMove/applicationSetup/webObjectCreatorController/webObjectCreatorControllerResponse")
    public GeneralResponseDto createWebObject(WebObjectDto webObjectDto){
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();
        dbWebObjectService.saveWebObject(new WebObject(webObjectDto));

        generalResponseDto.setDestination("createWebObject");
        generalResponseDto.setResponse("Web object created ".concat(webObjectDto.getWebName()));
        return generalResponseDto;
    }
    @MessageMapping("/applicationSetup/webObjectCreatorController/getAllWebObjects")
    @SendTo("/flowMove/applicationSetup/webObjectCreatorController/webObjectCreatorControllerResponse")
    public GeneralResponseDto getAllWebObjects( ){
        GeneralResponseDto generalResponseDto = new GeneralResponseDto();

        generalResponseDto.setDestination("getAllWebObjects");
        generalResponseDto.setResponse(dbWebObjectService.getAllWebObjects());
        return generalResponseDto;
    }
}
