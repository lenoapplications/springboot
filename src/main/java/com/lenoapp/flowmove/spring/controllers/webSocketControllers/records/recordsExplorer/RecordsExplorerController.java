package com.lenoapp.flowmove.spring.controllers.webSocketControllers.records.recordsExplorer;


import com.lenoapp.flowmove.records.recordsExplorer.RecordsExplorer;
import com.lenoapp.flowmove.records.recordsExplorer.recordInfo.RecordInfoHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

import java.util.HashMap;

@Controller
@MessageMapping("/recorder/recordsExplorer")
public class RecordsExplorerController {

    @Autowired
    RecordsExplorer recordsExplorer;

    @MessageMapping("/getAllTestRecords")
    @SendTo("/flowMove/getAllTestRecords")
    public HashMap<String, RecordInfoHolder> getAllRecords(){
        return recordsExplorer.getAllTestRecords();
    }
}
