function connectToServer(){
    window.mainObjectHolderForFlowMoveRecorder.flowMoveSocketRecorder.serverConnection.mainSocket = new WebSocket('$<url>');
    window.mainObjectHolderForFlowMoveRecorder.flowMoveSocketRecorder.serverConnection.clientStomp = Stomp.over(window.mainObjectHolderForFlowMoveRecorder.flowMoveSocketRecorder.serverConnection.mainSocket);
    console.log("connect To SERVER")

};

function setupWebSocketEvent(){
    window.mainObjectHolderForFlowMoveRecorder.flowMoveSocketRecorder.serverConnection.clientStomp.connect({},function(frame){
        console.log("Connected to web socket "+frame);
        window.mainObjectHolderForFlowMoveRecorder.flowMoveSocketRecorder.serverConnection.status = true;
    })
};

