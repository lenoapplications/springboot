function waitForPresenceOfElement(xpath){
    const webElement = window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.xpathFuncs.extractWebElement(xpath);
    return webElement !== null;
}
return waitForPresenceOfElement(arguments[0])