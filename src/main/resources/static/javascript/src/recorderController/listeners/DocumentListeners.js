function clickEvent(){
    document.addEventListener('click',function(event){
        if (window.mainObjectHolderForFlowMoveRecorder.flowMoveFlagsSignalization.searchForSelector){
            event.preventDefault();
            const endPoint = window.mainObjectHolderForFlowMoveRecorder.flowMoveStaticValues.webSocketMessageMappings.selectorClickedEvent;
            const xpath = window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.xpathFuncs.getXpathOfElement(event.target);
            window.mainObjectHolderForFlowMoveRecorder.flowMoveSocketRecorder.serverConnection.clientStomp.send(endPoint,{},xpath);
        }


    },true)
};
function createDocumentEventListeners(){
    clickEvent();
}