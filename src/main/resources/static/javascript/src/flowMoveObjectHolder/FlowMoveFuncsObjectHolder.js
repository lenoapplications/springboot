const flowMoveFunctionsRecorderObjectHolder = {
    utilz:{
        xpathFuncs:{
            extractWebElement:undefined,
            getXpathOfElement:undefined,
        },
        dtoSetupFuncs:{
            createDocumentDTO:undefined,
            createWindowDTO:undefined,
        }
    },
    socketCommunication:{
        sendEventToServer:undefined,
    }
};
