const flowMoveStaticValuesObjectHolder = {
    webSocketMessageMappings:{
        eventURLPath : '/app/events',
        assertURLPath : '/app/assert',
        windowUnloadURLPath : '/app/windowUnload',
        browserResponseJsCodeStatus : '/flowMove/browser/jsCodeStatus',
        browserRequestJsCodeStatus : '/app/jsCodeStatus',
        selectorClickedEvent : '/app/qualityCenter/testCreator/selectorController/selectorClicked'
    }
};