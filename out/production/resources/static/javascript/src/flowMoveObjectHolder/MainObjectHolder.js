window.mainObjectHolderForFlowMoveRecorder = {
    flowMoveFlagsSignalization:Object.assign({},flowMoveFlagsSignalizationObjectHolder),
    flowMoveFunctionsRecorder:Object.assign({},flowMoveFunctionsRecorderObjectHolder),
    flowMoveSocketRecorder:Object.assign({},flowMoveSocketRecorderObjectHolder),
    flowMoveStateRecorder:Object.assign({},flowMoveStateRecorderObjectHolder),
    flowMoveStaticValues:Object.assign({},flowMoveStaticValuesObjectHolder),
};
