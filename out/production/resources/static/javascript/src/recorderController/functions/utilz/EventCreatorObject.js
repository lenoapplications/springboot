function createDocumentEventObject(event,xpath,value,tag){
    var object = {
        eventOn:"document",
        event,
        xpath,
        value,
        tag,
    };
    return JSON.stringify(object);
};
function createWindowEventObject(event,windowName){
    var object = {
        eventOn:"window",
        event,
        windowName,
    };
    return JSON.stringify(object);
};

window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.dtoSetupFuncs.createDocumentDTO = createDocumentEventObject;
window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.dtoSetupFuncs.createWindowDTO = createWindowEventObject;
