function checkIfDocumentIsReady(){
    switch (document.readyState) {
        case "loading":return false;
        case "interactive":return false;
        case "complete":return true;
        default:return false;
    }
};
return checkIfDocumentIsReady();