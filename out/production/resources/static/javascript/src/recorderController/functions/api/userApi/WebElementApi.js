<GET_WEB_ELEMENT_STYLES>function getWebElementStyles(xpath){
    const webElement = window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.xpathFuncs.extractWebElement(xpath);
    const style = window.getComputedStyle(webElement);
    const mapOfStyle = {}
    for(var i = 0; i < style.length; i++){
        mapOfStyle[style[i]] = style.getPropertyValue(style[i]);
    }
    return mapOfStyle;
};</GET_WEB_ELEMENT_STYLES>

<SCROLL_INTO_VIEW_WEB_ELEMENT>function scrollIntoViewWebElement(webElement){
    console.log("scrolling into view",webElement);
    webElement.scrollIntoView(true);
}</SCROLL_INTO_VIEW_WEB_ELEMENT>