function sendEvent(event, target, value, tag){
    const xpath = window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.xpathFuncs.getXpathOfElement(target);
    var string = window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.dtoSetupFuncs.createDocumentDTO(event,xpath,value,tag);

    if (window.mainObjectHolderForFlowMoveRecorder.flowMoveFlagsSignalization.isAssertionActivated){
        window.mainObjectHolderForFlowMoveRecorder.flowMoveSocketRecorder.serverConnection.clientStomp.send(window.mainObjectHolderForFlowMoveRecorder.flowMoveStaticValues.webSocketMessageMappings.assertURLPath,{},string);
    }else{
        window.mainObjectHolderForFlowMoveRecorder.flowMoveSocketRecorder.serverConnection.clientStomp.send(window.mainObjectHolderForFlowMoveRecorder.flowMoveStaticValues.webSocketMessageMappings.eventURLPath,{},string);
    }

};
window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.socketCommunication.sendEventToServer = sendEvent;