function windowListenersSetup(){
    window.addEventListener("beforeunload",function(event){
        let windowEvent = window.mainObjectHolderForFlowMoveRecorder.flowMoveFunctionsRecorder.utilz.dtoSetupFuncs.createWindowDTO("beforeunload",
            window.mainObjectHolderForFlowMoveRecorder.flowMoveStateRecorder.windowStates.windowInformation.windowName);

        const urlPath = window.mainObjectHolderForFlowMoveRecorder.flowMoveStaticValues.webSocketMessageMappings.windowUnloadURLPath;
        window.mainObjectHolderForFlowMoveRecorder.flowMoveSocketRecorder.serverConnection.clientStomp.send(urlPath,{},windowEvent);
    })
}
